const User = require('../models/user');

function setUserInfo(request) {
    return {
        _id: request._id,
        username: request.profile.username,
        email: request.email,
    }
};

exports.getUserProfile = (req, res, next) => {
    const promise = User.findById(req.params.id).exec();
    promise.then(user => {
        res.json(user);
    })
        .catch(err => console.log(err));
};

exports.getAllUsers = (req, res, next) => {
   const query = req.query.name ? { 'profile.username': { $regex: `${ req.query.name }`, $options: 'i' } } : {};
    const promise = User.find(query, 'profile.username').sort({ 'profile.username': 'asc' }).exec();
    promise.then( users => {
        res.json(users);
    })
        .catch(err => {
            res.send(err);
        })
};

exports.updateProfile = (req, res, next) => {

  User.findOneAndUpdate({
    _id: req.params.id
  }, {
    $set: {
      'profileInfo.facebookUrl': req.body.facebookUrl,
      'profileInfo.twitterUrl': req.body.twitterUrl,
      'profileInfo.instagramUrl': req.body.instagramUrl,
      'profileInfo.linkedinUrl': req.body.linkedinUrl,
      'profileInfo.about': req.body.about,
      'profileInfo.interests': req.body.interests,
      'profileInfo.languages': req.body.languages,
      'profileInfo.city': req.body.city
    }
  }, {
    new: true
  }, function (err, userData) {
    if (err) {
      return res.send(err);
    }
    res.json({ profileInfo: userData.profileInfo, profile: { username: userData.profile.username } });
  });
};

exports.getProfileInterests = (req, res, next) => {
  User.findOne({ _id: req.params.id }, 'profile.username profileInfo', function (err, interests) {
    if (err) res.send(err);
    console.log(interests);
      res.json(interests);
  })
};

exports.uploadImage = (req, res, next) => {
  console.log(req.file, 'req.file');
  if (req.file && req.file.location) {
    res.json(req.file.location)
  } else {
    res.send('error');
  }
};

// exports.getUserPosts = (req, res, next) => {
//     const promise = User.findById(req.params.id).populate('posts').exec();
//     promise.then( user => {
//         res.json(user);
//     })
//         .catch(err => {
//             res.json(err);
//         })
// };