const Tag = require('../models/tag');

exports.findPostsByTag = (req, res, next) => {
  Tag.findOne({name: req.body.name})
    .populate('posts')
    .then((tag) => {
    res.json(tag.posts)
    })
    .catch(err => {
      res.json(err);
    })
};