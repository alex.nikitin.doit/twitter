const Post = require('../models/post');

exports.createPost = (req, res, next) => {
    const post = new Post({text : req.body.text, _user: req.params.id});
    const promise = post.save();
    promise.then(post => {
        res.json(post);
    })
        .catch(err => {
            res.json(err);
        })
};

exports.updatePost = (req, res, next) => {
  Post.findOneAndUpdate({
      _id: req.params.id
    },
    {
      $set: {
        'text': req.body.text
      }
    })
    .then(() => {
      res.send({message: 'OK'})
    })
    .catch(err => res.json(err));
};

exports.deletePost = (req, res, next) => {
  Post.findByIdAndRemove(req.params.id)
    .then(() => {
    res.status(200).json({message: 'OK'});
    })
    .catch(err => {
      res.json(err);
    })
};

exports.getPostById = (req, res, next) => {
    const promise = Post.findById(req.params.id).populate('_user').exec();
    promise.then( post => {
        res.json(post);
    })
        .catch( err => {
            res.json(err);
        })
};

exports.getPostsByUserId = (req, res, next) => {
  const promise = Post.find({_user:req.params.id}).populate({
    path: '_user',
    select: 'profile.username'
  }).sort('-createdAt').exec();
  promise.then(posts => {
      res.json(posts);
  })
      .catch(err => {
          res.json(err);
      })
};

exports.recentPosts = (req, res, next) => {
  const promise = Post.find().sort('-createdAt').limit(3).populate({
    path: '_user',
    select: 'profile.username'
  }).exec();
  promise.then(posts => res.json(posts))
    .catch(err => res.json(err))
};