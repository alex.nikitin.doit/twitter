const AuthenticationController = require('./controllers/authentication'),
    ProfileController = require('./controllers/profile'),
    PostController = require('./controllers/post'),
    ChatController = require('./controllers/chat'),
    express = require('express'),
    passportService = require('./config/passport'),
    passport = require('passport');

const uploadCampaignImage = require('./middleware/upload-profile-image-s3');

const requireAuth = passport.authenticate('jwt', { session: false });
const requireLogin = passport.authenticate('local', { session: false });

module.exports = function(app) {
    // Initializing route groups
    const apiRouter = express.Router(),
        authRouter = express.Router(),
        postRouter = express.Router(),
        chatRouter = express.Router();
        profileRouter = express.Router();

    //=========================
    // Auth Routes
    //=========================

    // Set auth routes as subgroup/middleware to apiRoutes
    apiRouter.use('/auth', authRouter);
    apiRouter.use('/profile', profileRouter);
    apiRouter.use('/post', postRouter);
    apiRouter.use('/chat', chatRouter);
    apiRouter.get('/me', requireAuth, AuthenticationController.me);

    authRouter.post('/register', AuthenticationController.register);
    authRouter.post('/login', requireLogin, AuthenticationController.login);


    profileRouter.get('/', ProfileController.getAllUsers);
    profileRouter.get('/:id', requireAuth, ProfileController.getUserProfile);
    profileRouter.get('/posts/:id', PostController.getPostsByUserId);
    profileRouter.get('/interests/:id', requireAuth, ProfileController.getProfileInterests);
    profileRouter.put('/upload-image', requireAuth, uploadCampaignImage, ProfileController.uploadImage);
    profileRouter.post('/:id', requireAuth, ProfileController.updateProfile);

    postRouter.get('/recent', PostController.recentPosts);
    postRouter.post('/:id', requireAuth, PostController.createPost);
    postRouter.get('/:id', requireAuth, PostController.getPostById);
    postRouter.delete('/:id', requireAuth, PostController.deletePost);
    postRouter.put('/:id', PostController.updatePost);

    // View messages to and from authenticated user
    chatRouter.get('/', requireAuth, ChatController.getConversations);

    // Retrieve single conversation
    chatRouter.get('/:conversationId', requireAuth, ChatController.getConversation);

    // Send reply in conversation
    chatRouter.post('/:conversationId', requireAuth, ChatController.sendReply);

    // Start new conversation
    chatRouter.post('/new/:recipient', requireAuth, ChatController.newConversation);


// Set url for API group routes
    app.use('/api', apiRouter);
};
