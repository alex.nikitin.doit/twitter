module.exports = function setUserInfo(request) {
    return {
        _id: request._id,
        username: request.profile.username,
        email: request.email,
        role: request.role
    }
};