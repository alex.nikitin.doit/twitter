const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
const randomstring = require('randomstring');
const config = require('config');
const mime = require('mime');

let s3;

module.exports = (req, res, next) => {
  if (!s3) {
    s3 = new AWS.S3({ signatureVersion: 'v4' });
  }
  return multer({
    storage: multerS3({
      s3,
      bucket: 'admin-streetster-bucket',
      acl: 'public-read',
      contentDisposition: 'inline',
      contentType(reqest, file, cb) {
        cb(null, file.mimetype);
      },
      metadata(reqest, file, cb) {
        cb(null, {
          fieldName: file.fieldname
        });
      },
      key(reqest, file, cb) {
        if (!['image/png', 'image/jpeg'].includes(file.mimetype)) {
          const error = {
            error: {
              message: 'Invalid file type. Only .jpg and .png images are allowed.'
            }
          };
          res.send(error);
          cb(error);
          return;
        }
        cb(null, `${'campaign-img/' + randomstring.generate(16)}.${mime.extension(file.mimetype)}`);
      }
    })
  }).single('backgroundImage')(req, res, next);
};
