const express = require('express'),
      app = express(),
      bodyParser = require('body-parser'),
      logger = require('morgan'),
      mongoose = require('mongoose'),
      socketEvents = require('./socketEvents'),
      config = require('./config/main'),
      AWS = require('aws-sdk');

const router = require('./router');

//Database connection
mongoose.Promise = require('bluebird');
mongoose.connect(config.database);

// Amazon SDK Config
AWS.config.update({
  accessKeyId: 'AKIAIUCXY74EHXCE6VKQ',
  secretAccessKey: '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',
  region: 'us-west-2'
});
// AWS RequestTimeTooSkewed error workaround (https://github.com/aws/aws-sdk-js/issues/399#issuecomment-233057244)
AWS.events.on('retry', (response) => {
  if (response.error.name === 'RequestTimeTooSkewed') {
    console.error('User time is not correct. Handling error!');
    console.log('AWS systemClockOffset:', AWS.config.systemClockOffset);
    const serverTime = Date.parse(response.httpResponse.headers.date);
    const timeNow = new Date().getTime();
    console.log('AWS timestamp:', new Date(serverTime));
    console.log('Browser timestamp:', new Date(timeNow));
    AWS.config.systemClockOffset = Math.abs(timeNow - serverTime);
    response.error.retryable = true;
    console.log('Setting systemClockOffset to:', AWS.config.systemClockOffset);
    console.log('Retrying uploading to S3 once more...');
  }
});

const server = app.listen(config.port);
console.log('Your server is running on port', config.port);

const io = require('socket.io').listen(server);
socketEvents(io);

app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
});

router(app);

// example
