const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt-nodejs');

const ProfileInfoSchema = new Schema({
  facebookUrl: {
    type: String,
    default: ''
  },
  twitterUrl: {
    type: String,
    default: ''
  },
  linkedinUrl: {
    type: String,
    default: ''
  },
  instagramUrl: {
    type: String,
    default: ''
  },
  about: {
    type: String,
    default: ''
  },
  city: {
    type: String,
    default: ''
  },
  languages: {
    type: String,
    default: ''
  },
  interests: {
    type: String,
    default: ''
  }
});

const UserSchema = new Schema({
   email: {
       type: String,
       lowercase: true,
       unique: true,
       required: true
   },
    password: {
        type: String,
        required: true
    },
    profile: {
        username: { type: String }
    },
    role: {
        type: String,
        enum: ['Member', 'Client', 'Owner', 'Admin'],
        default: 'Member'
    },
    posts: [{
       type: Schema.Types.ObjectId,
       ref: 'Post'
    }],
    profileInfo: {
     type: ProfileInfoSchema,
     default: ProfileInfoSchema
    },
    resetPasswordToken: { type: String },
    resetPasswordExpires: { type: Date }
},
{
    timestamps: true
});

UserSchema.pre('save', function (next) {
   const user = this,
       SALT_FACTOR = 5;

   if (!user.isModified('password')) return next();

   bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
       if (err) return next(err);

       bcrypt.hash(user.password, salt, null, function (err, hash) {
           if (err) return next(err);
           user.password = hash;
           next();
       });
   });
});

UserSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
      if (err) { return cb(err); }

      cb(null, isMatch);
  })
};

module.exports = mongoose.model('User', UserSchema);