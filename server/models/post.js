const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema({
   text: {
       type: String,
       required: true
   },
    _user: {
       type: Schema.Types.ObjectId,
       ref: 'User'
    },
    tags: {
     type: Schema.Types.ObjectId,
      ref: 'Tag'
    }
},
{
    timestamps: true
});

module.exports = mongoose.model('Post', PostSchema);