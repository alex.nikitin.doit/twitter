import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import 'babel-polyfill';

import rootReducer from 'reducers';
import Routes from 'routes';

// Load SCSS
import '../scss/index.scss'


const isProduction = process.env.NODE_ENV === 'production';

// Creating store
export let store = null;

if (isProduction) {
    // In production adding only thunk middleware
   const middleware = applyMiddleware(thunk);

    store = createStore(
        rootReducer,
        middleware,
    );
} else {
    // In development mode beside thunk
    const middleware = applyMiddleware(thunk);


    store = createStore(
        rootReducer,
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
        middleware
    );
}


// Render it to DOM
ReactDOM.render(
<Provider store={ store }>
    { isProduction ?
    <Routes /> :
<div>
<Routes />
</div> }
</Provider>,
    document.getElementById('root')
);