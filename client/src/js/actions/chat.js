import io from 'socket.io-client';
import {
  FETCH_CONVERSATIONS_START,
  FETCH_CONVERSATIONS,
  FETCH_CONVERSATION_START,
  FETCH_CONVERSATION,
  START_CONVERSATION_START,
  START_CONVERSATION,
  SEND_MESSAGE_START,
  SEND_MESSAGE,
  CHAT_ERROR
} from './types';
import { deleteData, postData, getData, putData } from './index';
export const socket = io.connect('http://localhost:3000');

export const fetchConversations = () => {
  const url = '/chat';
  return dispatch => getData(FETCH_CONVERSATIONS_START, FETCH_CONVERSATIONS, CHAT_ERROR, true, url, dispatch);
};

export const fetchConversation = (recipientId) => {
  const url = `/chat/${recipientId}`;
  return dispatch => getData(FETCH_CONVERSATION_START, FETCH_CONVERSATION, CHAT_ERROR, true, url, dispatch);
};

export function startConversation (recipientId, data) {
  const url = `/chat/new/${recipientId}`;
  return dispatch => postData(START_CONVERSATION_START, START_CONVERSATION, CHAT_ERROR, true, url, dispatch, { composedMessage: data });
}

export function sendReply (conversationId, data) {
  const url = `/chat/${conversationId}`;
  return dispatch => {
    return postData(SEND_MESSAGE_START, SEND_MESSAGE, CHAT_ERROR, true, url, dispatch, { composedMessage: data })
      .then(() => {
      setTimeout(() => {
        socket.emit('new message', conversationId);
      }, 100);
      });
  };
}
