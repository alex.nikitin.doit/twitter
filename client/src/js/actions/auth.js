import axios from 'axios';
import cookie from 'react-cookie';
import { API_URL, CLIENT_ROOT_ROOT, errorHandler } from './index'
import { AUTH_USER, AUTH_ERROR, UNAUTH_USER, FORGOT_PASSWORD_REQUEST, RESET_PASSWORD_REQUEST } from './types'
import { browserHistory } from 'react-router';


export function loginUser({email, password}) {
    return function (dispatch) {
        axios.post(`${API_URL}/auth/login`, {username: email, password})
            .then((res) => {
                cookie.save('token', res.data.token, { path: '/' });
                cookie.save('userId', res.data.user._id);
                dispatch({
                    type: AUTH_USER,
                    payload:res.data.user
                });
                browserHistory.push('/');
                // window.location.href = `${CLIENT_ROOT_ROOT}/home`;
            })
            .catch((error) => {
                errorHandler(dispatch, error.response, AUTH_ERROR);
            })
    };
}

export function me() {
    return function (dispatch) {
        axios.get(`${API_URL}/me`, {
            headers: { Authorization: cookie.load('token') }
        })
            .then( (res) => {
                dispatch({
                    type: AUTH_USER,
                    payload: res.data.user
                })
            })
            .catch( (error) => {
                cookie.remove('token');
                browserHistory.replace('/login');
                errorHandler(dispatch, error.response, AUTH_ERROR);
            })
    }
}

export function registerUser({email, username, password}) {
    return function (dispatch) {
        axios.post(`${API_URL}/auth/register`, {email, username, password})
            .then((res) => {
            cookie.save('token', res.data.token, { path: '/' });
            cookie.save('user', res.data.user, { path: '/'});
            cookie.save('userId', res.data.user._id);
                dispatch({
                    type: AUTH_USER,
                    payload:res.data.user
                });
                browserHistory.push(`/`);
            })
            .catch((err) => {
            errorHandler(dispatch, err.response, AUTH_ERROR);
            });
    };
}

export function logoutUser(error) {
    return function (dispatch) {
        dispatch({ type: UNAUTH_USER, payload: error || '' });
        cookie.remove('token', { path: '/' });
        cookie.remove('user', { path: '/' });

        window.location.href = `${CLIENT_ROOT_ROOT}/login`;
    };
}