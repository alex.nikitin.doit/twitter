import {
  START_PROFILE_SUBMIT,
  PROFILE_SUBMIT,
  START_PROFILE_INTERESTS,
  PROFILE_INTERESTS,
  PROFILE_ERROR,
  PROFILE_SEARCH_START,
  PROFILE_SEARCH
} from './types';
import { deleteData, postData, getData, putData } from './index';


export function submitProfile(id, data) {
  const url = `/profile/${id}`;
  return dispatch => postData(START_PROFILE_SUBMIT, PROFILE_SUBMIT, PROFILE_ERROR, true, url, dispatch, data);
}

export function profileInterests(id) {
  const url = `/profile/interests/${id}`;
  return dispatch => getData(START_PROFILE_INTERESTS, PROFILE_INTERESTS, PROFILE_ERROR, true, url, dispatch);
}

export function getUserByName(name) {
  const url = name.length > 0 ? `/profile?name=${name}` : `/profile`;
  return dispatch => getData(PROFILE_SEARCH_START, PROFILE_SEARCH, PROFILE_ERROR, true, url, dispatch);
}


