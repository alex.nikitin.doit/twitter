import {
  CREATE_AVATAR_START,
  CREATE_AVATAR,
  USER_ERROR
} from './types';

import {
  putData
} from './index';

export function uploadAvatar(uri) {
  const url = '/profile/upload-image';
  const post = {
    backgroundImage: uri
  };
  return dispatch => putData(CREATE_AVATAR_START, CREATE_AVATAR, USER_ERROR, true, url, dispatch, post);
}
