import { getData, postData, putData, deleteData } from './index';
import {
  CREATE_POST,
  CREATE_POST_START,
  POST_ERROR,
  GET_POSTS,
  GET_POSTS_START,
  DELETE_POST_START,
  DELETE_POST,
  RECENT_POST_START,
  RECENT_POST,
  UPDATE_POST_START,
  UPDATE_POST
} from './types'

export function getPosts(userId) {
  const url = `/profile/posts/${userId}`;
  return dispatch  => getData(GET_POSTS_START, GET_POSTS, POST_ERROR, true, url, dispatch);
}

export function createPost(text, userId) {
  console.log(userId, 'user idd');
  const url = `/post/${userId}`;
  const post = {
    text: text,
    _user: userId
  };
  return dispatch => postData(CREATE_POST_START, CREATE_POST, POST_ERROR, true, url, dispatch, post);
}

export function deletePost(postId) {
  const url = `/post/${postId}`;
  return dispatch => deleteData(DELETE_POST_START, DELETE_POST, POST_ERROR, true, url, dispatch);
}

export function updatePost(postId, text) {
  const url = `/post/${postId}`;
  const postData = {
    text: text
  };
  return dispatch => putData(UPDATE_POST_START, UPDATE_POST, POST_ERROR, true, url, dispatch, postData);
}

export function recentPosts() {
  const url = '/post/recent';
  return dispatch => getData(RECENT_POST_START, RECENT_POST, POST_ERROR, true, url, dispatch);
}
