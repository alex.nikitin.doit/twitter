//= =====================
// Auth Actions
//= =====================
export const AUTH_USER = 'auth_user',
    UNAUTH_USER = 'unauth_user',
    AUTH_ERROR = 'auth_error',
    FORGOT_PASSWORD_REQUEST = 'forgot_password_request',
    RESET_PASSWORD_REQUEST = 'reset_password_request',
    AUTH_START = 'auth_start',

//= =====================
// Post Actions
//= =====================
    CREATE_POST = 'create_post',
    CREATE_POST_START = 'create_post_start',
    GET_POSTS = 'get_posts',
    GET_POSTS_START = 'get_posts_start',
    DELETE_POST_START= 'delete_post_start',
    DELETE_POST = 'delete_post',
    RECENT_POST_START = 'recent_post_start',
    RECENT_POST = 'recent_post',
    UPDATE_POST_START = 'update_post_start',
    UPDATE_POST = 'update_post',
    POST_ERROR = 'post_error',

//= =====================
// Profile Actions
//= =====================
    START_PROFILE_SUBMIT = 'start_profile_submit',
    PROFILE_SUBMIT = 'profile_submit',
    START_PROFILE_INTERESTS = 'start_profile_interests',
    PROFILE_INTERESTS = 'profile_interests',
    PROFILE_SEARCH_START = 'profile_search_start',
    PROFILE_SEARCH = 'profile_search',
    PROFILE_ERROR = 'profile_error',

//= =====================
// Chat Actions
//= =====================
    FETCH_CONVERSATIONS_START = 'get_conversations_start',
    FETCH_CONVERSATIONS = 'get_conversations',

    FETCH_CONVERSATION_START = 'get_conversation_start',
    FETCH_CONVERSATION = 'get_conversation',

    START_CONVERSATION = 'start_conversation',
    START_CONVERSATION_START = 'start_conversation_start',

    SEND_MESSAGE_START = 'send_message_start',
    SEND_MESSAGE = 'send_message',

    CHAT_ERROR = 'chat_error';

//= =====================
// User Actions
//= =====================
export const CREATE_AVATAR_START = 'create_avatar_start';
export const CREATE_AVATAR = 'create_avatar';

export const USER_ERROR = 'user_error';
