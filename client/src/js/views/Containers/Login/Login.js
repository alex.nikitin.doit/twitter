import React, { Component, PropTypes } from 'react';
import Paper from 'material-ui/Paper';
import SwipeableViews from 'react-swipeable-views';
import {
  Tabs,
  Tab
} from 'material-ui/Tabs';
import {
  LoginForm,
  RegistrationForm
} from '../../Components';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { slideIndex: 0 };
  }

  handleChange = slideIndex => this.setState({ slideIndex });

  render() {
    const { slideIndex } = this.state;

    return (
      <div id="login-container">
        <Paper zDepth={3} style={{marginTop: '50px'}}>
          <div className="login_form_wrapper">
            <Tabs onChange={this.handleChange} value={slideIndex}>
              <Tab label="Login" value={0}/>
              <Tab label="Registration" value={1}/>
            </Tabs>
            <SwipeableViews index={slideIndex} >
              <LoginForm active={slideIndex === 0} />
              <RegistrationForm active={slideIndex === 1} />
            </SwipeableViews>
          </div>
        </Paper>
      </div>
    )
  }
}