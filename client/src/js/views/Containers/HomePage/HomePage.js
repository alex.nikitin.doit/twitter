import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
  Post,
  NewestPost,
  StartChatModal
} from '../../Components';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import { Link } from 'react-router'
import {
  getPosts,
  createPost,
  deletePost,
  recentPosts
} from '../../../actions/post'
import cookie from 'react-cookie'

const avatarImage = require('../../../../assets/img/alex.jpg');
import {
  Avatar
} from '../../Components';

@connect(state => ({
    user: state.auth.user,
    posts: state.post.data,
    postLoading: state.post.loading,
    profileLoading: state.auth.loading,
    actualPosts: state.post.recentPosts
}),
  {
  getPosts,
  createPost,
  deletePost,
  recentPosts
  })

export default class HomePage extends Component {
    static propTypes = {
        user: PropTypes.object,
        posts: PropTypes.object
    };

    constructor(props) {
        super(props);
        this.state = {
          postText: '',
          modalOpened: false
        }
    }

    toggleModal = () => {
      this.setState({
        modalOpened: !this.state.modalOpened
      });
    };

    componentDidMount() {
      const { getPosts, recentPosts } = this.props;
      if (this.props.params.id) {
        getPosts(this.props.params.id);
      } else {
        getPosts(cookie.load('userId'));
      }
      recentPosts();
    }
    sendPost() {
      const { createPost, user : { _id }, getPosts } = this.props;
      const { postText } = this.state;
      const self = this;
      if (postText.length > 0) {
        createPost(postText, _id)
          .then(() => {
            getPosts(_id);
            self.setState({postText: ''});
          })
          .catch(err => console.log(err));
      }
    }
    deletePost = (id) => {
      const { deletePost, user : { _id }, getPosts } = this.props;
      deletePost(id)
        .then(() => {
          getPosts(_id);
        })
    };

    render() {
        const { user, posts, actualPosts, params } = this.props;
        const { postText, modalOpened } = this.state;
        const isOtherProfile = this.props.params.id && (this.props.params.id !== cookie.load('userId'));

        return (
            <div id="home_page">
              <div className="home_page_container">
                <div className="left_profile_block">
                  <Avatar />
                  {params.id && (
                    <StartChatModal
                      userId={params.id}
                      toggleModal={this.toggleModal}
                      modalOpened={modalOpened}
                    />
                  )}
                  <div className="profile_info_block">
                    <div className="tweets_count_block">
                      <div className="tweets">TWEETS</div>
                      <div className="count">{ posts.length }</div>
                    </div>
                    <div className="readers_count_block">
                      <div className="readers">READERS</div>
                      <div className="count">1</div>
                    </div>
                  </div>
                  <div style={{display: 'flex', justifyContent: 'space-around', width: '100%'}}>
                    {params.id && (
                      <RaisedButton
                        label="Send message"
                        secondary={false}
                        onClick={this.toggleModal}
                      />
                    )}
                    <Link to={`/profile/${!isOtherProfile ? cookie.load('userId') : this.props.params.id}`}><RaisedButton label="Profile" secondary={true}/></Link>
                  </div>
                </div>
                   <div className="center_post_block">
                  { !isOtherProfile &&
                  <div className="post_textarea">
                  <TextField
                    floatingLabelText="What's new?"
                    multiLine={true}
                    rows={1}
                    fullWidth={true}
                    value={postText}
                    onChange={(e) => this.setState({postText: e.target.value})}
                  />
                  <div className="send_post_btn" onClick={() => this.sendPost(user._id)}>
                  <i className="post_img"></i>
                  </div>
                  </div> }
                  {
                    !!posts.length && posts.map((post, item) => {
                      return <Post isOtherProfile={isOtherProfile} post={post} key={item} deletePost={this.deletePost}/>
                    })
                  }
                  </div>

                <div className="right_recent_post_block">
                  <div className="title">Recent posts</div>
                  {!!actualPosts.length && actualPosts.map((post, item) => {
                    return <NewestPost post={post} key={item}/>
                  })}
                </div>
              </div>
            </div>
        )
    }
}