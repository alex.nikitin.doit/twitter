import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { ChatGroupSider } from '../../Components';

import {
  socket,
  fetchConversations,
  fetchConversation
} from '../../../actions/chat';

@connect(state => ({
  conversations: state.chat.conversations
}), {
  fetchConversations,
  fetchConversation
})

export default class Chat extends Component {
  constructor(props) {
    super(props);
    const { params } = this.props;

    if (params.id) {
      socket.emit('enter conversation', params.id);
      socket.on('refresh messages', () => {
        console.log('new message')
        this.props.fetchConversation(params.id);
      });
    }
  }

  componentWillMount() {
    this.props.fetchConversations()
  }

  render() {
    const {
      params,
      fetchConversation,
      conversations
    } = this.props;

    return (
      <div className="chat-main">
        <ChatGroupSider
          paramsId={params && params.id}
          fetchConversation={ fetchConversation }
          conversations={ conversations }
        />
        { params.id ? this.props.children : (
          <div className="chat-main__choose-chat choose-chat">
            Please select a chat to start messaging
          </div>
        )}
      </div>
    )
  }
}
