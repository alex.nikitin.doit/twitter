import React, {Component, PropTypes} from 'react'
import { Link } from 'react-router'
import cookie from 'react-cookie';
import { connect } from 'react-redux'
import { Throttle } from 'react-throttle';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';

import Avatar from 'material-ui/Avatar';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';

import { getUserByName } from '../../../actions/profile';

import injectTapEventPlugin from 'react-tap-event-plugin';

const avatarImage = require('../../../../assets/img/alex.jpg');

@connect( state => ({
  profiles: state.profile.profiles
}), {
  getUserByName
})


export default class App extends Component {
    static PropTypes = {
    };

    constructor(props) {
        super(props);

        this.state = {
            isProfileSidebarOpened: false,
            open: false
        }
    }
    componentWillMount() {
        injectTapEventPlugin();
    }

    handleOpen = () => {
      this.setState({open: true});
    };

    handleClose = () => {
      this.setState({open: false});
    };

    handleCloseProfileSidebar = () => {
        this.setState({
            isProfileSidebarOpened: false
        });
    };
    handleOpenProfileSidebar = () => {
        this.setState({
            isProfileSidebarOpened: true
        });
    };

    render() {
      const style = {margin: 5};
      const { profiles, getUserByName } = this.props;
      const actions = [
        <FlatButton
          label="Ok"
          primary={true}
          keyboardFocused={true}
          onTouchTap={this.handleClose}
        />,
      ];
        const {children} = this.props;
        const userId = cookie.load('userId');
        return (
            <MuiThemeProvider>
                <div id="app-container">
                    <AppBar title="Steetster"
                            className="main-navbar"
                            onLeftIconButtonTouchTap={this.handleOpenProfileSidebar}
                            iconElementRight={<RaisedButton style={{marginTop: '6px', marginRight: '15px'}} primary={true}
                                                            label="Search"
                                                            onTouchTap={this.handleOpen} />}/>
                    <Drawer
                        docked={false}
                        width={260}
                        open={this.state.isProfileSidebarOpened}
                        onRequestChange={(isProfileSidebarOpened) => this.setState({isProfileSidebarOpened})}
                    >
                      <Link to="/" style={{ textDecoration: 'none' }}><MenuItem onTouchTap={this.handleCloseProfileSidebar}>Home</MenuItem></Link>
                      <Link to={`/profile/${userId}`} style={{ textDecoration: 'none' }}><MenuItem onTouchTap={this.handleCloseProfileSidebar}>Profile</MenuItem></Link>
                      <Link to="/about" style={{ textDecoration: 'none' }}><MenuItem onTouchTap={this.handleCloseProfileSidebar}>About</MenuItem></Link>
                      <Link to="/chat"><MenuItem onTouchTap={this.handleCloseProfileSidebar}>Chat</MenuItem></Link>
                    </Drawer>
                  <Dialog
                    title="Profile search"
                    actions={actions}
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                    autoScrollBodyContent={true}
                  >
                    <div id="search_profile_block">
                      <Throttle time="3000" handler="onChange">
                      <TextField
                        hintText=""
                        floatingLabelText="Enter username"
                        onChange={(e) => { getUserByName(e.target.value)}}
                      />
                      </Throttle>
                    </div>
                    <List>
                      { profiles.length > 0 && profiles.map((item, i) => {
                        return (
                          <a href={`http://localhost:4000/home/${item._id}`}><ListItem
                            key={i}
                            leftAvatar={
                              <Avatar src={avatarImage} />
                            }
                          >
                            {item.profile.username}
                          </ListItem></a>
                        )
                      }) }
                    </List>
                  </Dialog>
                    <div className="page-container">
                        {children}
                    </div>
                </div>
            </MuiThemeProvider>
        )
    }
}