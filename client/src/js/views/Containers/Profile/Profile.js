import React , { Component, PropTypes} from 'react';
import Slider from 'react-slick'
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import cookie from 'react-cookie';
import {
  TextField,
} from 'redux-form-material-ui'
import { submitProfile, profileInterests } from '../../../actions/profile'
import RaisedButton from 'material-ui/RaisedButton';
import {
  StartChatModal
} from '../../Components';

const alexPhoto = require('../../../../assets/img/alex.jpg');
const alexCapPhoto = require('../../../../assets/img/alex_cap.jpg');
const iconFacebook = require('../../../../assets/img/facebook.png');
const iconTwitter = require('../../../../assets/img/twitter.png');
const iconLinkedin = require('../../../../assets/img/linkedin.png');
const iconInstagram = require('../../../../assets/img/instagram.png');
const iconInterests = require('../../../../assets/img/interests.png');
const iconAbout = require('../../../../assets/img/boy.png');
const iconLanguage = require('../../../../assets/img/language.png');
const iconCity = require('../../../../assets/img/home.png');
const leftChevron = require('../../../../assets/img/chevron-pointing-to-the-left.png');
const rightChevron = require('../../../../assets/img/right-chevron.png');

class Profile extends Component {
    static PropTypes = {
    };

    constructor(props) {
      super(props);
      this.next = this.next.bind(this);
      this.previous = this.previous.bind(this);
      this.state = {
        editableProfile: false,
        modalOpened: false
      }
    }

  componentDidMount() {
      const { profileInterests, params: { id } } = this.props;
      profileInterests(id);
  }

  next() {
    this.slider.slickNext();
  }

  previous() {
    this.slider.slickPrev();
  }

  toggleModal = () => {
      this.setState({
        modalOpened: !this.state.modalOpened
      })
  };

  handleEditBtn = () => {
    this.setState({
      editableProfile: true
    });
  };
  editProfile = (data) => {
    const userId = cookie.load('userId');
    const { submitProfile } = this.props;
    submitProfile(userId, data);
    this.setState({
      editableProfile: false
    });
  };


    render() {
      const { editableProfile, modalOpened } = this.state;
      const { handleSubmit, initialValues, profile, user, params } = this.props;
      console.log(initialValues, 'initialValues');
      const currentUserId = cookie.load('userId');
      const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: <LeftArrow prev={this.previous} />,
        nextArrow: <RightArrow next={this.next} />
      };
        return (
          <form onSubmit={handleSubmit(this.editProfile)}>
            <div id="profile_page">
              <StartChatModal
                toggleModal={this.toggleModal}
                modalOpened={modalOpened}
              />
              <div className="profile_page_container">
                <div className="left_profile_container">
                  <div className="profile_block">
                    <div className="carousel_container">
                      <Slider ref={c => this.slider = c } {...settings}>
                        <div><CarouselItem src={alexCapPhoto}/></div>
                        <div><CarouselItem src={alexPhoto}/></div>
                      </Slider>
                    </div>
                    {profile && profile.profile.username && (<div className="username">{ profile.profile.username }</div>)}
                    <div className="age">29 years old</div>
                    {currentUserId !== params.id && (
                      <RaisedButton
                        label="Send message"
                        secondary={false}
                        onClick={this.toggleModal}
                      />
                    )}
                  </div>
                  <div className="social_block">
                    <div className="social_icons_block">
                      {editableProfile ? <Field name="facebookUrl" component={TextField} hintText="facebook"/> :
                      <div className="social-row"><div className="title">Facebook</div>
                        <a href={ profile && profile.profileInfo.facebookUrl ? profile.profileInfo.facebookUrl: ''}><img className="social_icon" src={iconFacebook}/></a></div> }
                    </div>
                    <div className="social_icons_block">
                      {editableProfile ? <Field name="twitterUrl" component={TextField} hintText="twitter"/> :
                        <div className="social-row"><div className="title">Twitter</div>
                          <a href={ profile && profile.profileInfo.twitterUrl ? profile.profileInfo.twitterUrl: ''}><img className="social_icon" src={iconTwitter}/></a></div> }
                    </div>
                    <div className="social_icons_block">
                      {editableProfile ? <Field name="linkedinUrl" component={TextField} hintText="Linkenin"/> :
                        <div className="social-row"><div className="title">Linkedin</div>
                          <a href={ profile && profile.profileInfo.linkedinUrl ? profile.profileInfo.linkedinUrl : ''}><img className="social_icon" src={iconLinkedin}/></a></div> }
                    </div>
                    <div className="social_icons_block">
                      {editableProfile ? <Field name="instagramUrl" component={TextField} hintText="Instagram"/> :
                        <div className="social-row"><div className="title">Instagram</div>
                          <a href={ profile && profile.profileInfo.instagramUrl ? profile.profileInfo.instagramUrl : ''}><img className="social_icon" src={iconInstagram}/></a></div> }
                    </div>
                  </div>
                </div>
                <div className="right_profile_container">
                  { !editableProfile && <div className="edit_profile_btn" onClick={this.handleEditBtn}>EDIT</div> }
                  <div className="info_block">
                    <div className="title_block"><img className="profile_icon" src={iconAbout}/><span className="title">About me</span></div>
                    { editableProfile ? <Field name="about" component={TextField} hintText="Tell about yourself"/> :
                      <div className="description">{ profile && profile.profileInfo.about ? profile.profileInfo.about : 'no description provided' }</div>}
                  </div>
                  <div className="info_block">
                    <div className="title_block"><img className="profile_icon" src={iconCity}/><span className="title">City</span></div>
                    { editableProfile ? <Field name="city" component={TextField} hintText="Your city"/> :
                      <div className="description">{ profile && profile.profileInfo.city ? profile.profileInfo.city : 'no description provided' }</div> }
                  </div>
                  <div className="info_block">
                    <div className="title_block"><img className="profile_icon" src={iconLanguage}/><span className="title">Languages</span></div>
                    { editableProfile ? <Field name="languages" component={TextField} hintText="Languages"/> :
                      <div className="description">{ profile && profile.profileInfo.languages ? profile.profileInfo.languages : 'no description provided' }</div> }
                  </div>
                  <div className="info_block">
                    <div className="title_block"><img className="profile_icon" src={iconInterests}/><span className="title">Interests</span></div>
                    { editableProfile ? <Field name="interests" component={TextField} hintText="Interests"/> :
                      <div className="description">{ profile && profile.profileInfo.interests ? profile.profileInfo.interests : 'no description provided' }</div> }
                  </div>
                  { editableProfile && <div className="edit_profile_btn" style={{marginTop: '20px'}} onClick={handleSubmit(this.editProfile)}>SUBMIT</div> }
                </div>
              </div>
            </div>
          </form>
        )
    }
}

class CarouselItem extends Component {
  render() {
    return (<div><img className="carousel_avatar_image" {...this.props} /></div>)
  }
}

class RightArrow extends Component {
  render() {
    return (
      <div className="arrow__next carousel-right-arrow" onClick={this.props.next}><img src={rightChevron} className="angle-right"/></div>
    );
  }
}

class LeftArrow extends Component {
  render() {
    return (
      <div className="arrow__prev carousel-left-arrow" onClick={this.props.prev}><img src={leftChevron} className="angle-left"/></div>
    );
  }
}

Profile = reduxForm({
  form: 'profileForm'
})(Profile);

Profile = connect(
  state => ({
    initialValues: state.profile.info,
    profile: state.profile.data
  }),
  { profileInterests, submitProfile }
)(Profile);


export default Profile