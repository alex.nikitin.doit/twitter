import React, { Component } from "react";
import AvatarCropper from '../Сropper/cropper';
import Dialog from 'material-ui/Dialog';
import { connect } from 'react-redux';

import {
  uploadAvatar
} from '../../../actions/user';
@connect(state => ({}), { uploadAvatar })

export default class Avatar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cropperOpen: false,
      img: null,
      croppedImg: "http://www.fillmurray.com/400/400"
    }
  }

  handleFileChange = (dataURI) => {
    this.setState({
      img: dataURI,
      croppedImg: this.state.croppedImg,
      cropperOpen: true
    });
  };

  handleCrop = (dataURI) => {
    this.setState({
      cropperOpen: false,
      img: null,
      croppedImg: dataURI
    });

    this.props.uploadAvatar(dataURI);
  };

  handleRequestHide = () => {
    this.setState({
      cropperOpen: false
    });
  };

  handleFile = (e) => {
    const reader = new FileReader();
    const file = e.target.files[0];

    if (!file) return;

    reader.onload = (img) => {
      this.refs.in.value = '';
      this.handleFileChange(img.target.result);
    };
    reader.readAsDataURL(file);
  };

  render() {
    return (
      <div>
        <div className="avatar-photo">
          <input ref="in" type="file" accept="image/*" onChange={this.handleFile} />
          <div className="avatar-edit">
            <span>Click to Pick Avatar</span>
            <i className="fa fa-camera"></i>
          </div>
          <img src={this.state.croppedImg}/>
        </div>
        {this.state.cropperOpen && (
        <Dialog
          modal={true}
          open={this.state.cropperOpen}
          onRequestClose={this.handleRequestHide}
          contentStyle={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}
        >
          <AvatarCropper
            onRequestHide={this.handleRequestHide}
            cropperOpen={this.state.cropperOpen}
            onCrop={this.handleCrop}
            image={this.state.img}
            width={400}
            height={400}
          />
        </Dialog>
        )}
      </div>
    );
  }
}
