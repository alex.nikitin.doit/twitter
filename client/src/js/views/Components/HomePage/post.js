import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import {Card, CardActions, CardHeader, CardTitle, CardText} from 'material-ui/Card';
import { updatePost, getPosts } from '../../../actions/post';
import cookie from 'react-cookie'
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';

const AvatarImg = require('../../../../assets/img/default-avatar-250x250.png');

@connect(state => ({
  }),
  {
    updatePost,
    getPosts
  })

export default class Post extends Component {
  static propTypes = {
    user: PropTypes.object
  };
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      editablePost: false,
      editableText: props.post.text
    };
  }

  handleTouchTap = (event) => {
    // This prevents ghost click.
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  deletePost = (id) => {
    const { deletePost } = this.props;
    deletePost(id);
    this.setState({
      open: false
    })
  };

  updatePost = (id) => {
    const { editableText } = this.state;
    const { updatePost, getPosts } = this.props;
    const self = this;
    updatePost(id, editableText)
      .then(() => {
        return getPosts(cookie.load('userId'));
      })
      .then(() => {
        self.setState({
          editablePost: false,
          open: false
        })
      })
      .catch(err => console.log(err));
  };



  render() {
    const { post, isOtherProfile } = this.props;
    const { editablePost, editableText } = this.state;
    console.log(!editablePost && !isOtherProfile, '!editablePost && !isOtherProfile');
    return (
    <div className="post_block">
      <Card>
        <CardHeader
          title={post._user.profile.username}
          subtitle={moment(post.createdAt).fromNow()}
          avatar={AvatarImg}
        />
        <CardTitle subtitle="#silver #moloco #colon" />
        {editablePost ? (<div className="post_edit_block">
          <TextField name="edit"
                     multiLine={true}
                     rows={2}
                     rowsMax={4}
                     defaultValue={editableText}
                     onChange={(e) => this.setState({editableText: e.target.value})}/>
          <RaisedButton onTouchTap={ () => this.updatePost(post._id) } label="update"/>
        </div>): <CardText>{post.text}</CardText>}
        <CardActions style={cardStyle}>
          { !editablePost && !isOtherProfile && <RaisedButton
            onTouchTap={this.handleTouchTap}
            label="Actions"
            primary={true}
          />}
          {!editablePost && <Popover
            open={this.state.open}
            anchorEl={this.state.anchorEl}
            anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
            targetOrigin={{horizontal: 'left', vertical: 'top'}}
            onRequestClose={this.handleRequestClose}
          >
            <Menu>
              <MenuItem primaryText="Edit" onTouchTap={() => this.setState({editablePost: true})}/>
              <MenuItem onTouchTap={() => this.deletePost(post._id)} primaryText="Delete" />
            </Menu>
          </Popover>}
        </CardActions>
      </Card>
    </div>
    )
  }
}

const cardStyle = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'flex-end'
};
