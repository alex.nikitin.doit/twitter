import React, { Component } from 'react';
import moment from 'moment';
const avatarImage = require('../../../../assets/img/alex.jpg');

class NewestPost extends Component {
  render() {
    const { post } = this.props;
    console.log(post, 'recent post');
    return (
      <div className="newest_post">
        <img className="avatar_img" src={ avatarImage } alt="avatar"/>
        <div className="content_info_block">
          <div className="username_time_block">
            <span className="username">{post._user.profile.username}</span>
            <span className="time">{ moment(post.createdAt).fromNow() }</span>
          </div>
          <div className="post_text">{ post.text }</div>
        </div>
      </div>
    )
  }
}

export default NewestPost