/*_______________Authorization_______________*/
export LoginForm from './Login/loginForm';
export RegistrationForm from './Login/registrationForm';

/*_______________Post_______________*/
export Post from './HomePage/post';
export NewestPost from './HomePage/newestPost';

/*_______________Chat_______________*/
export ChatGroupSider from './Chat/ChatGroupSider';
export ChatContent from './Chat/ChatContent';
export StartChatModal from './Chat/StartChatModal';

/*_______________Profile_______________*/
export Avatar from './Profile/Avatar';
