import React, { Component } from 'react';
import { Link } from 'react-router';
import moment from 'moment';
const avatarImage = require('../../../../assets/img/alex.jpg');

const GroupItem = ({conversation, active, activate}) => (
  <Link to={`/chat/${conversation.conversationId}`}>
    <div className={`sider_item item ${active ? 'active' : ''}`}>
      <img className="item__avatar" src={avatarImage}/>
      <div className="item__info">
        <span>{conversation.author.profile.username}</span>
        <span>{moment(conversation.createdAt).fromNow()}</span>
        <span>{conversation.body}</span>
      </div>
    </div>
  </Link>
);

export default class ChatGroupSider extends Component {
  render() {
    const { conversations, paramsId } = this.props;
    return (
      <div className="chat-main__sider sider">
        { conversations.map((conversation, i) => (
          <GroupItem
            key={i}
            conversation={conversation[0]}
            active={paramsId === conversation[0].conversationId}
          />
        )) }
      </div>
    )
  }
}