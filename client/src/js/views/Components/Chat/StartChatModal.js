import React, { Component } from 'react'
import Dialog from 'material-ui/Dialog';
import Textarea from 'react-textarea-autosize';
import ContentSend from 'material-ui/svg-icons/content/send';
import IconButton from 'material-ui/IconButton';
import { connect } from 'react-redux';
import { startConversation } from '../../../actions/chat';

@connect(state => ({
}), {
  startConversation
})

export default class StartChatModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: ''
    };
  }

  onStartConversation = () => {
    const { message } = this.state;
    if (!message) return;
    const { userId, startConversation } = this.props;
    startConversation(userId, message)
      .then(this.props.toggleModal());
  };

  render() {
    const { modalOpened, toggleModal } = this.props;
    return (
      <Dialog
        className="start-chat-modal"
        title={<div className="title">Send a message <span onClick={toggleModal}>close</span></div>}
        modal={true}
        open={modalOpened}
        onRequestClose={toggleModal}
        contentStyle={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}
      >
        <form className="start-chat-modal__content" onSubmit={this.onStartConversation}>

          <Textarea
            minRows={5}
            maxRows={8}
            placeholder="Type a message here.."
            autoFocus
            onChange={e => this.setState({message: e.target.value})}
          />

          <IconButton onClick={this.onStartConversation}>
            <ContentSend />
          </IconButton>
        </form>
      </Dialog>
    )
  }
}
