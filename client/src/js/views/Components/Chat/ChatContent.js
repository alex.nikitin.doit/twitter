import React, { Component } from 'react';
import Textarea from 'react-textarea-autosize';
import ContentSend from 'material-ui/svg-icons/content/send';
import IconButton from 'material-ui/IconButton';
import { connect } from 'react-redux';
import cookie from 'react-cookie';
import moment from 'moment';
import {
  sendReply,
  fetchConversation
} from '../../../actions/chat';

const BubbleMessage = ({message, from, time}) => (
  <div className={`chat__bubble bubble ${from}`}>
    {message}
    <span className="bubble__time">{moment(time).fromNow()}</span>
  </div>
);

@connect(state => ({
  conversations: state.chat.conversations,
  conversation: state.chat.conversation
}), ({
  sendReply,
  fetchConversation
}))

export default class ChatContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: ''
    };
  }

  onSendReply = () => {
    const { message } = this.state;
    message && this.props.sendReply(this.props.params.id, message);
  };

  componentDidUpdate() {
    const chat_block = document.querySelector('.chat');
    const last_bubble = document.querySelectorAll('.bubble');

    chat_block.scrollTop = chat_block.scrollHeight + last_bubble[last_bubble.length-1].offsetHeight;
  }

  render() {
    const { conversation } = this.props;

    return (
      <div className="chat-main__content-messages content-messages">
        <div className="content-messages__chat chat">
          {conversation && conversation.map((mess, i) => {
            const from = mess.author._id === cookie.load('userId') ? 'me' : 'you';
            return (
              <BubbleMessage
                message={mess.body}
                from={from}
                time={mess.createdAt}
                key={i}
              />
            )
          })}
        </div>
        <div className="content-messages__input input">
          <Textarea
            minRows={3}
            maxRows={6}
            onChange={e => this.setState({ message: e.target.value })}
            placeholder="Type a message here.."
            autoFocus
          />
          <IconButton onClick={this.onSendReply}>
            <ContentSend />
          </IconButton>
        </div>
      </div>
    )
  }
}
