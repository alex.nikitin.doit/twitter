import React, { Component, PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import RaisedButton from 'material-ui/RaisedButton';
import { connect } from 'react-redux';
import { loginUser } from '../../../actions/auth'

const required = value => value == null ? 'Required' : undefined;
const email = value => value &&
!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? 'Invalid email' : undefined;
const password = value => value.toString().length > 15 ? 'More than 15 characters' : undefined;

@connect(() => ({}), {loginUser})

class LoginForm extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.refs.email            // the Field
            .getRenderedComponent() // on Field, returns ReduxFormMaterialUITextField
            .getRenderedComponent() // on ReduxFormMaterialUITextField, returns TextField
            .focus();                // on TextField
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.active) this.props.reset();
    }

    render() {
        const { handleSubmit, pristine, reset, submitting, loginUser } = this.props;
        return (
          <div className="login-form">
                <form onSubmit={handleSubmit}>
                    <div>
                        <Field name="email"
                        component={TextField}
                        floatingLabelText="Email"
                        validate={[required, email]}
                        ref="email" withRef/>
                    </div>
                    <div>
                        <Field name="password"
                               type="password"
                               component={TextField}
                               floatingLabelText="password"
                               validate={[required, password]}
                               />
                    </div>
                    <div className="submit-buttons">
                        <RaisedButton onTouchTap={handleSubmit(loginUser)}
                                      label="LOGIN"
                                      labelColor="#FFFFFF"
                                      backgroundColor="#EF6D3B"
                                      disabled={submitting}/>
                        <RaisedButton onTouchTap={reset}
                                      label="CLEAR"
                                      labelColor="#FFFFFF"
                                      backgroundColor="#7A9A95"
                                      disabled={pristine || submitting}/>
                    </div>
                </form>
          </div>
        )
    }
}
export default reduxForm({
    form: 'loginForm'
})(LoginForm)