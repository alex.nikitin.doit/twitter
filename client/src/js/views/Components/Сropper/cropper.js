import React from "react";
import ReactDom from "react-dom";
import {isDataURL} from "./utils";
import Slider from 'material-ui/Slider';
import FlatButton from 'material-ui/FlatButton';

class Cropper extends React.Component {

  constructor () {
    super();

    // getInitialState
    this.state = {
      dragging: false,
      image: {},
      mouse: {
        x: null,
        y: null
      },
      preview: null,
      zoom: 1
    };

    this.listeners = [];
  }

  fitImageToCanvas (width, height) {
    let scaledHeight
      , scaledWidth;

    const canvasAspectRatio = this.props.height / this.props.width;
    const imageAspectRatio = height / width;

    if (canvasAspectRatio > imageAspectRatio) {
      scaledHeight = this.props.height;
      let scaleRatio = scaledHeight / height;
      scaledWidth = width * scaleRatio;
    } else {
      scaledWidth = this.props.width;
      let scaleRatio = scaledWidth / width;
      scaledHeight = height * scaleRatio;
    }

    return { width: scaledWidth, height: scaledHeight };
  }

  prepareImage (imageUri) {
    const img = new Image();
    if (!isDataURL(imageUri)) img.crossOrigin = 'anonymous';
    img.onload = () => {
      const scaledImage = this.fitImageToCanvas(img.width, img.height);
      scaledImage.resource = img;
      scaledImage.x = 0;
      scaledImage.y = 0;
      this.setState({dragging: false, image: scaledImage, preview: this.toDataURL()});
    };
    img.src = imageUri;
  }

  mouseDownListener (e) {
    this.setState({
      image: this.state.image,
      dragging: true,
      mouse: {
        x: null,
        y: null
      }
    });
  }

  preventSelection (e) {
    if (this.state.dragging) {
      e.preventDefault();
      return false;
    }
  }

  mouseUpListener (e) {
    this.setState({ dragging: false, preview: this.toDataURL() });
  }

  mouseMoveListener (e) {
    if (!this.state.dragging) return;

    const mouseX = e.clientX;
    const mouseY = e.clientY;
    const imageX = this.state.image.x;
    const imageY = this.state.image.y;

    const newImage = this.state.image;

    if (this.state.mouse.x && this.state.mouse.y) {
      const dx = this.state.mouse.x - mouseX;
      const dy = this.state.mouse.y - mouseY;

      const bounded = this.boundedCoords(imageX, imageY, dx, dy);

      newImage.x = bounded.x;
      newImage.y = bounded.y;
    }

    this.setState({
      image: this.state.image,
      mouse: {
        x: mouseX,
        y: mouseY
      }
    });
  }

  boundedCoords (x, y, dx, dy) {
    const newX = x - dx;
    const newY = y - dy;

    const scaledWidth = this.state.image.width * this.state.zoom;
    const dw = (scaledWidth - this.state.image.width) / 2;

    const rightEdge = this.props.width;

    if (newX - dw > 0) { x = dw; }
    else if (newX < (-scaledWidth + rightEdge)) { x = rightEdge - scaledWidth; }
    else {
      x = newX;
    }

    const scaledHeight = this.state.image.height * this.state.zoom;
    const dh = (scaledHeight - this.state.image.height) / 2;

    const bottomEdge = this.props.height;

    if (newY - dh > 0) {
      y = dh;
    }
    else if (newY < (-scaledHeight + bottomEdge)) {
      y = bottomEdge - scaledHeight;
    }
    else {
      y = newY;
    }

    return { x: x, y: y };
  }

  componentDidMount () {
    const canvas = ReactDom.findDOMNode(this.refs.canvas);
    this.prepareImage(this.props.image);

    this.listeners = {
      mousemove: e => this.mouseMoveListener(e),
      mouseup: e => this.mouseUpListener(e),
      mousedown: e => this.mouseDownListener(e)
    };

    window.addEventListener("mousemove", this.listeners.mousemove, false);
    window.addEventListener("mouseup", this.listeners.mouseup, false);
    canvas.addEventListener("mousedown", this.listeners.mousedown, false);
    document.onselectstart = e => this.preventSelection(e);
  }

  // make sure we clean up listeners when unmounted.
  componentWillUnmount () {
    const canvas = ReactDom.findDOMNode(this.refs.canvas);
    window.removeEventListener("mousemove", this.listeners.mousemove);
    window.removeEventListener("mouseup", this.listeners.mouseup);
    canvas.removeEventListener("mousedown", this.listeners.mousedown);
  }

  componentDidUpdate () {
    const context = ReactDom.findDOMNode(this.refs.canvas).getContext("2d");
    context.clearRect(0, 0, this.props.width, this.props.height);
    this.addImageToCanvas(context, this.state.image);
  }

  addImageToCanvas (context, image) {
    if (!image.resource) return;
    context.save();
    context.globalCompositeOperation = "destination-over";
    const scaledWidth = this.state.image.width * this.state.zoom;
    const scaledHeight = this.state.image.height * this.state.zoom;

    let x = image.x - (scaledWidth - this.state.image.width) / 2;
    let y = image.y - (scaledHeight - this.state.image.height) / 2;

    // need to make sure we aren't going out of bounds here...
    x = Math.min(x, 0);
    y = Math.min(y, 0);
    y = scaledHeight + y >= this.props.height ? y : (y + (this.props.height - (scaledHeight + y)));
    x = scaledWidth + x >= this.props.width ? x : (x + (this.props.width - (scaledWidth + x)));

    context.drawImage( image.resource, x, y, image.width * this.state.zoom, image.height * this.state.zoom);
    context.restore();
  }

  toDataURL () {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");

    canvas.width = this.props.width;
    canvas.height = this.props.height;

    this.addImageToCanvas(context, {
      resource: this.state.image.resource,
      x: this.state.image.x,
      y: this.state.image.y,
      height: this.state.image.height,
      width: this.state.image.width
    });

    return canvas.toDataURL();
  }

  handleCrop () {
    this.props.onCrop(this.toDataURL());
  }

  render () {
    const { width, height, onRequestHide } = this.props;
    return (
      <div className="AvatarCropper-canvas">
        <div className="row">
          <canvas
            ref="canvas"
            width={width}
            height={height}>
          </canvas>
        </div>

        <div className="row">
          <Slider
            onChange={this.handleZoomUpdate.bind(this)}
            style={{width}}
            min={1}
            max={3}
            step={0.01}
            defaultValue={1}
          />
        </div>

        <div className='modal-footer'>
          <FlatButton
            label="Save"
            secondary={true}
            onClick={this.handleCrop.bind(this)}
          />
          <FlatButton
            label="Cancel"
            primary={true}
            onClick={onRequestHide.bind(this)}
          />
        </div>

      </div>
    );
  }

  handleZoomUpdate (event, value) {
    this.setState({ zoom: value });
  }
}
Cropper.defaultProps = { width: 400, height: 400, zoom: 1 };

class AvatarCropper extends React.Component {
  constructor () {
    super();
  }

  render () {
    return (
      <Cropper
        image={this.props.image}
        width={this.props.width}
        height={this.props.height}
        onCrop={this.props.onCrop}
        onRequestHide={this.props.onRequestHide}
        closeButtonCopy={this.props.closeButtonCopy}
        cropButtonCopy={this.props.cropButtonCopy}
      />
    );
  }
}

AvatarCropper.propTypes = {
  image: React.PropTypes.string.isRequired,
  onCrop: React.PropTypes.func.isRequired,
  closeButtonCopy: React.PropTypes.string,
  cropButtonCopy: React.PropTypes.string,
  modalSize: React.PropTypes.oneOf(["lg", "large", "sm", "small"]),
  onRequestHide: React.PropTypes.func.isRequired
};
AvatarCropper.defaultProps = { width: 400, height: 400, modalSize: "large",
  closeButtonCopy: "Close", cropButtonCopy: "Crop and Save"};

export default AvatarCropper;
