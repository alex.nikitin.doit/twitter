import React, { Component } from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import {store} from './index'
import { me } from './actions/auth'
import cookie from 'react-cookie'

import App from 'views/Containers/App/App';
import Dashboard from 'views/Containers/Dashboard/Dashboard';
import HomePage from 'views/Containers/HomePage/HomePage';
import About from './views/Containers/About'
import Profile from './views/Containers/Profile/Profile'
import Login from './views/Containers/Login/Login'
import NotFound from './views/Containers/NotFound/NotFound'
import Chat from './views/Containers/Chat/Chat';
import { ChatContent } from './views/Components';

import { fetchConversation } from './actions/chat';

const publicPath = '/';

export const routeCodes = {
    DASHBOARD: `${publicPath}dashboard`,
    ABOUT: `${ publicPath }about`,
    HOME: `${ publicPath }:userId`,
    PROFILE: `${ publicPath }profile`,
    LOGIN: `${ publicPath }login`
};

export default class Routes extends Component {

    requireAuth = (nextState, replace, cb) => {
        const { auth: { user } } = store.getState();
        if (!user) {
            store.dispatch(me());
            cb();
        }
        cb();
    };

    checkAccess = (nextState, replace, cb) => {
        if (cookie.load('token')) {
            replace('/');
            cb();
        }
        cb();
    };

    getConversation = (nextState, replace, cb) => {
      store.dispatch(fetchConversation(nextState.params.id));
      cb();
    };

    render() {
        return (
            <Router history={ browserHistory }>
                <Route path={ publicPath } component={ App }>

                    <Route onEnter={this.requireAuth}>
                      <IndexRoute component={ HomePage } />
                      <Route path='/home/:id' component={HomePage}/>
                      <Route path='/dashboard' component={ Dashboard } />
                      <Route path='/about' component={About}/>
                      <Route path='/profile/:id' component={Profile}/>
                      <Route path='/chat' component={Chat}>
                        <Route path=":id" component={ChatContent} onEnter={this.getConversation}/>
                      </Route>
                    </Route>

                    <Route onEnter={this.checkAccess} path='/login' component={Login}/>
                    <Route onEnter={this.requireAuth} path="*" component={NotFound} />
                </Route>
            </Router>
        );
    }
}