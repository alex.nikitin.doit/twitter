import {
  CREATE_AVATAR_START,
  CREATE_AVATAR,
  USER_ERROR
} from '../actions/types';

const initialState = {
  user: null,
  isLoading: false,
  message: '',
  error: ''
};

export default function (state = initialState, action = {}) {
  switch (action.type) {
    case CREATE_AVATAR_START:
      return {
        ...state,
        avatar: action.payload,
        isLoading: false
      };
    case CREATE_AVATAR:
      return {
        ...state,
        isLoading: true,
      };
    case USER_ERROR:
      return {
        ...state,
        message: action.message,
        error: action.error
      };
  }

  return state;
}