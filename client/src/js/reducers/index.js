import { combineReducers } from 'redux';
import auth from 'reducers/auth'
import post from 'reducers/post';
import profile from 'reducers/profile';
import chat from 'reducers/chat';
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
    auth,
    post,
    profile,
    chat,
    form: formReducer
});