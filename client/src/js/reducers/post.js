import {
  CREATE_POST,
  CREATE_POST_SUCCESS,
  POST_ERROR,
  GET_POSTS,
  GET_POSTS_START,
  DELETE_POST_START,
  DELETE_POST,
  RECENT_POST_START,
  RECENT_POST,
  UPDATE_POST_START,
  UPDATE_POST
} from '../actions/types';

const initialState = {
  loading: false,
  data: [],
  recentPosts: [],
  errorPost: ''
};

export default function (state = initialState, action = {}) {
  switch (action.type) {
    case GET_POSTS_START:
      return {
        ...state,
        loading: true
      };
    case GET_POSTS:
      return {
        ...state,
        loading: false,
        data: action.payload
      };
    case DELETE_POST_START:
      return {
        ...state,
        loading: true
      };
    case DELETE_POST:
      return {
        ...state,
        loading: false
      };
    case RECENT_POST_START:
      return {
        ...state,
        loading: true
      };
    case RECENT_POST:
      return {
        ...state,
        loading: false,
        recentPosts: action.payload
      };
    case UPDATE_POST_START:
      return {
        ...state,
        loading: true
      };
    case UPDATE_POST:
      return {
        ...state,
        loading: false
      }
  }
  return state;
}