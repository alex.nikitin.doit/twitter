import {
    AUTH_ERROR,
    AUTH_START,
    AUTH_USER,
    FORGOT_PASSWORD_REQUEST,
    PROTECTED_TEST,
    RESET_PASSWORD_REQUEST,
    UNAUTH_USER
} from '../actions/types';

const initialState = {
    user: null,
    loading: false,
    message: '',
    error: ''
};

export default function (state = initialState, action = {}) {
    switch (action.type) {
        case AUTH_USER:
            return {
                ...state,
                user: action.payload,
              loading: false
            };
        case AUTH_START:
            return {
                ...state,
              loading: true,
            };
        case AUTH_ERROR:
            return {
                ...state,
                message: action.message,
                error: action.error
            };
    }

    return state;
}