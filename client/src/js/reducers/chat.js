import {
  FETCH_CONVERSATIONS_START,
  FETCH_CONVERSATIONS,
  FETCH_CONVERSATION_START,
  FETCH_CONVERSATION,
  START_CONVERSATION_START,
  START_CONVERSATION,
  SEND_MESSAGE_START,
  SEND_MESSAGE,
  CHAT_ERROR
} from '../actions/types';

const initialState = {
  loading: false,
  conversations: [],
  conversation: [],
  errorChat: ''
};

export default function (state = initialState, action = {}) {
  switch (action.type) {
    case FETCH_CONVERSATIONS_START:
      return {
        ...state,
        loading: true
      };
    case FETCH_CONVERSATIONS:
      return {
        ...state,
        loading: false,
        conversations: action.payload.conversations
      };
    case FETCH_CONVERSATION_START:
      return {
        ...state,
        loading: true
      };
    case FETCH_CONVERSATION:
      return {
        ...state,
        loading: false,
        conversation: action.payload.conversation
      };
    case START_CONVERSATION_START:
      return {
        ...state,
        loading: true
      };
    case START_CONVERSATION:
      return {
        ...state,
        loading: false
      };
    case SEND_MESSAGE_START:
      return {
        ...state,
        loading: true
      };
    case SEND_MESSAGE:
      return {
        ...state,
        loading: false
      };
    case CHAT_ERROR:
      return {
        ...state,
        loading: false,
        errorChat: action.payload
      }
  }
  return state;
}