import {
  START_PROFILE_SUBMIT,
  PROFILE_SUBMIT,
  START_PROFILE_INTERESTS,
  PROFILE_INTERESTS,
  PROFILE_SEARCH_START,
  PROFILE_SEARCH,
  PROFILE_ERROR } from '../actions/types';

const initialState = {
  loading: false,
  data: null,
  errorProfile: '',
  info: null,
  profiles: []
};

export default function (state = initialState, action = {}) {
  switch (action.type) {
    case START_PROFILE_SUBMIT:
      return {
        ...state,
        loading: true
      };
    case PROFILE_SUBMIT:
      return {
        ...state,
        data: action.payload,
        info: action.payload.profileInfo,
        loading: false
      };
    case START_PROFILE_INTERESTS:
      return {
        ...state,
        loading: true,
      };
    case PROFILE_INTERESTS:
      return {
        ...state,
        data: action.payload,
        info: action.payload.profileInfo,
        loading: false
      };
    case PROFILE_SEARCH_START:
      return {
        ...state,
        loading:true
      };
    case PROFILE_SEARCH:
      return {
        ...state,
        loading: false,
        profiles: action.payload
      }
  }
  return state;
}



